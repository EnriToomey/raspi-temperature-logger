import datetime
from django.http import JsonResponse, Http404
from django.shortcuts import HttpResponse
from django.utils.timezone import localtime, get_current_timezone_name
from temperature_logger.models import Temperature, Humidity


def last_temperature_json(request):
    temperature = Temperature.objects.last()
    if temperature:
        if temperature.was_published_recently:
            return JsonResponse({
                'celsius': temperature.celsius,
                'fahrenheit': temperature.fahrenheit,
            })
        else:
            raise Http404("The last temperature meassurement is too all")
    else:
        raise Http404("There's no temperature meassurements")


def last_humidity_json(request):
    humidity = Humidity.objects.last()
    if humidity:
        if humidity.was_published_recently:
            return JsonResponse({
                'humidity': humidity.humidity,
            })
        else:
            raise Http404("The last humidity meassurement is too all")
    else:
        raise Http404("There's no humidity meassurements")


def last_measurement_json(request):
    json_dict = {}
    temperature = Temperature.objects.last()
    if temperature:
        if temperature.was_published_recently:
            json_dict.update({
                'celsius': temperature.celsius,
                'fahrenheit': temperature.fahrenheit,
            })
    humidity = Humidity.objects.last()
    if humidity:
        if humidity.was_published_recently:
            json_dict.update({
                'humidity': humidity.humidity,
            })
    return JsonResponse(json_dict)


def measurements_plot(request, last_hours=None):
    import numpy as np
    import bokeh.plotting
    from bokeh.resources import CDN
    from bokeh.embed import file_html

    N = int(request.GET.get("N", 1))
    M = int(request.GET.get("M", 1))

    timezone_name = get_current_timezone_name()
    TOOLS = "pan,xwheel_zoom,box_zoom,reset,save,xpan,xwheel_pan,hover"

    p1 = bokeh.plotting.figure(
        title="Temperature",
        tools=TOOLS,
        x_axis_type='datetime',
        x_axis_label='Date [{}]'.format(timezone_name),
        y_axis_label='Temperature [°C]',
        plot_width=1000, plot_height=400,
    )
    p2 = bokeh.plotting.figure(
        title="Humidity",
        tools=TOOLS,
        x_axis_type='datetime',
        x_axis_label='Date [{}]'.format(timezone_name),
        y_axis_label='Humidity [%]',
        plot_width=1000, plot_height=400,
    )

    for sensor_id, color, legend in [(20, 'blue', 'interior'), (21, 'red', 'exterior')]:
        if last_hours:
            last_date = Temperature.objects.last().pub_date
            since = last_date - datetime.timedelta(hours=last_hours)
            query_temperature = Temperature.objects.filter(pub_date__gte=since, sensor=sensor_id)
            query_humidity = Humidity.objects.filter(pub_date__gte=since, sensor=sensor_id)
        else:
            query_temperature = Temperature.objects.filter(sensor=sensor_id)
            query_humidity = Humidity.objects.filter(sensor=sensor_id)

        temperatures = np.array(query_temperature.values_list('pub_date', 'celsius'))
        x, y = temperatures[::M, 0], temperatures[:, 1]

        x = np.array(list(map(localtime, x)))
        y = np.convolve(y, np.ones(N), 'valid') / N
        y = y[::M]

        p1.circle(x, y, color=color)
        p1.line(x, y, color=color, legend="{} - GPIO {}".format(legend, sensor_id))

        humidities = np.array(query_humidity.values_list('pub_date', 'humidity'))
        x, y = humidities[::M, 0], humidities[:, 1]
        x = np.array(list(map(localtime, x)))
        y = np.convolve(y, np.ones(N), 'valid') / N
        y = y[::M]

        p2.circle(x, y, color=color)
        p2.line(x, y, color=color, legend="{} - GPIO {}".format(legend, sensor_id))

    p = bokeh.plotting.gridplot([[p1], [p2]], sizing_mode='stretch_both')

    html = file_html(p, CDN, "Temperatures & Humidity")
    return HttpResponse(html)
