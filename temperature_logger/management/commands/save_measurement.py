from django.core.management.base import BaseCommand, CommandError
from temperature_logger.models import Temperature, Humidity
from temperature_logger.sensor import read_humidity_sensor

# I harcoded the sensor ID and the GPIO ID because I did not understand how
# django commands handled arguments. The naive asumption that everying after the command name
# would be taken as argument, stores in argv list, did not seemed to work.
SENSOR_ID = 11
GPIO_LIST = [20, 21]

class Command(BaseCommand):
    help = 'Save a new temperature measurement'

    def handle(self, *args, **options):
        for GPIO in GPIO_LIST:
            try:
                humidity, celsius = read_humidity_sensor(SENSOR_ID, GPIO)
                print(humidity, celsius, GPIO)
            except ValueError:
                raise CommandError("Not expected value from the sensor")
            else:
                Temperature.objects.create(celsius=celsius, sensor=GPIO)
                Humidity.objects.create(humidity=humidity, sensor=GPIO)
