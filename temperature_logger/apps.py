from django.apps import AppConfig


class TemperatureLoggerConfig(AppConfig):
    name = 'temperature_logger'
