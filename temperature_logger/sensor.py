import Adafruit_DHT
import sys

WAIT_INTERVAL = 0.2

sensor_args = {11: Adafruit_DHT.DHT11,
               22: Adafruit_DHT.DHT22,
               2302: Adafruit_DHT.AM2302}
rpi_gpios = [4, 5, 6, 12, 13, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27]


def read_humidity_sensor(sensor_id, gpio):
    if sensor_id not in sensor_args.keys():
        sys.exit("Sensor {} does not existe".format(sensor_id))
    if gpio not in rpi_gpios:
        sys.exit("Gpio {} does not existe".format(gpio))
    sensor = sensor_args[sensor_id]
    return Adafruit_DHT.read_retry(sensor, gpio)