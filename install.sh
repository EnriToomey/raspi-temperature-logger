#!/bin/sh

set -x

cp debian/* /lib/systemd/system
systemctl daemon-reload
systemctl enable raspi_temperature_logger_web_server.service
systemctl enable raspi_temperature_logger_save_measurement.timer
